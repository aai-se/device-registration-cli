@ECHO OFF
ECHO Starting Device Registration , needs 1. Control Room-URL 2. User Name 3. User's API Key
TITLE Device Registration
REM set java path , e.g. JRE of the Bot Agent
set JAVA="C:\Program Files\Automation Anywhere\Bot Agent\jre\bin\java" 
REM set Bot Agent app data path
set APPDATA="C:\Windows\System32\config\systemprofile\AppData\Local\AutomationAnywhere"
REM set registration jar file location
set REGLIB="registration.jar"
REM set security jar file location 
set SECLIB="C:\Program Files\Automation Anywhere\Bot Agent\lib\bc-fips-1.0.2.jar"
REM set default user
set DEFUSER=%4
REM run registration command
IF DEFINED DEFUSER ( 
   %JAVA% -cp %SECLIB%;%REGLIB% registration.Main -c %1 -u %2 -a %3 -p %APPDATA% -du %DEFUSER%
) ELSE (
  %JAVA% -cp %SECLIB%;%REGLIB% registration.Main -c %1 -u %2 -a %3 -p %APPDATA% 
)
